@extends('faq::layouts.master')

@section('content')
    <h1>FAQ</h1>

    @foreach ($categories as $category)
    <h2>{{ $category->name }}</h2>
    @forelse ( $category->questions as $question)
    <h3>{{ $question->question_text }}</h3>
    <p>{{ $question->answer_text }}</p>
    @empty
    No Questions in this Category!
    @endforelse
    <hr />
    @endforeach
    @stop
    <!-- <p>
        This view is loaded from module: {!! config('faq.name') !!}
    </p> -->
