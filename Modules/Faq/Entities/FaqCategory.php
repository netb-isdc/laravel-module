<?php

namespace Modules\Faq\Entities;

use Modules\Faq\Entities\FaqQuestion;
use Illuminate\Database\Eloquent\Model;

class FaqCategory extends Model
{
    protected $fillable = ['name'];

    public function questions()
    {
        return $this->hasMany( FaqQuestion::class, 'faq_category_id');
    }
}
