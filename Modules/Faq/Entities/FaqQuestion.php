<?php

namespace Modules\Faq\Entities;

use Modules\Faq\Entities\FaqCategory;
use Illuminate\Database\Eloquent\Model;

class FaqQuestion extends Model
{
    protected $fillable = ['faq_category_id', 'question_text', 'answer_text'];

    public function category()
    {
        return $this->belongsTo( FaqCategory::class, 'faq_category_id');
    }
}
