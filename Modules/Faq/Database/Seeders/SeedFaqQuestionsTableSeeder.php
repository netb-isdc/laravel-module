<?php

namespace Modules\Faq\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Faq\Entities\FaqCategory;
use Illuminate\Database\Eloquent\Model;


class SeedFaqQuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call("OthersTableSeeder");
        $categories = [
            [
                'name'=>'Registration',
                'questions' => [
                    [
                        'question_text'=> 'How to Register?',
                        'answer_text' => 'Click on your Butt-on',
                    ],
                    [
                        'question_text' => 'How long is your Banana?',
                        'answer_text' => '12398 meters',
                    ],
                ]
                ],
                [
                    'name'=> 'Plans & Payments',
                    'questions' => [
                        [
                            'question_text' => 'How can I Pay?',
                            'answer_text' => 'Use Gcash',
                        ],
                        [
                            'question_text' => 'What is Love?',
                            'answer_text' => 'Love is Blind',
                        ],
                    ]
                    ],
        ];
        foreach($categories as $category){
            $new_category = FaqCategory::create(['name'=> $category['name']]);
            foreach ($category['questions'] as $question){
                $new_category->questions()->create($question);

            }
        }
    }
}
